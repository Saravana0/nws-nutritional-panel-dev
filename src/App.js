import React, { useEffect } from "react";
import "react-bootstrap-table2-toolkit/dist/react-bootstrap-table2-toolkit.min.css";
import "react-bootstrap-table2-paginator/dist/react-bootstrap-table2-paginator.min.css";
import "./App.css";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  withRouter,
  useLocation,
} from "react-router-dom";
import { PrivateRoute } from "./Library";

// import { connect } from "react-redux";
// import { getUserData } from "./store/actions";

import Header from "./components/header";

import LoginPage from "./pages/login";
import ForgetpswdPage from "./pages/forgetpswd";
import ResetpswdPage from "./pages/resetpswd";
import NotfoundPage from "./pages/notfound";
import ManageUserProgress from "./pages/manageuserprogress";
import MealTracker from "./pages/mealtracker";
import UserChat from "./pages/userchat";

export default function App() {
  return (
    <>
      <Router>
        <Header />
        <Switch>
          <Route exact path="/" component={LoginPage} />
          <Route
            exact
            path="/forgot-password"
            component={withRouter(ForgetpswdPage)}
          />
          <Route
            exact
            path="/reset-password/:token"
            component={withRouter(ResetpswdPage)}
          />
          <Route
            exact
            path="/manage-user-progress"
            component={withRouter(ManageUserProgress)}
          />
          <Route
            exact
            path="/meal-tracker/"
            component={withRouter(MealTracker)}
          />
          <Route exact path="/user-chat/" component={withRouter(UserChat)} />
          <Route path="*" component={NotfoundPage} />
        </Switch>
      </Router>
    </>
  );
}

// const mapStateToProps = (state) => {
//   console.log(state);
//   return {
//     user: state.user,
//   };
// };

// const mapDispatchToProps = {
//   updateUser: getUserData,
// };

// export default connect(mapStateToProps, mapDispatchToProps)(App);
