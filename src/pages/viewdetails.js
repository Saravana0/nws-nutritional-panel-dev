import React, { useEffect, useState, useRef } from "react";
import "../App.css";
import { Link, Switch } from "react-router-dom";
import Close from "../assets/images/closeimg.png";
// import Img1 from "../assets/images/image1.png";
import Modal from "react-modal";
import { Button, Form, Col, Row, Tabs, Tab } from "react-bootstrap";
import { LineChart } from "./manageuserprogress";
import { Bar } from "react-chartjs-3";
import { Line } from "react-chartjs-3";
import { ProgressTrackerBody } from "../pages/manageuserprogress";
import ModalPopDietpanelMenu from "../components/DietPlan/dietpanelmenu";
import { RiCloseFill } from "react-icons/ri";
// import DietPanel from "../components/DietPlan/dietpanel";

import {
  BarChart,
  Bar as Recharts,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
} from "recharts";

import {
  BrowserRouter as Router,
  // Switch,
  Route,
  withRouter,
  useLocation,
} from "react-router-dom";

/*-------------------stepCountData------------------------ */
const stepCountData = {
  data: {
    data: [20, 18, 16, 14, 12, 10, 8, 6, 4, 2, 0],
    labels: [
      "PT W1",
      "PT W2",
      "W1",
      "W2",
      "W3",
      "W4",
      "W5",
      "W6",
      "W7",
      "W8",
      "W9",
      "W10",
      "W11",
      "W12",
      "W13",
    ],
    datasets: [
      {
        label: "DAY 1",
        data: [10, 12, 8],
        barThickness: 12,
      },
      {
        label: "DAY 2",
        data: [12, 12, 8],
        barThickness: 12,
      },
      {
        label: "DAY 3",
        data: [8, 8, 14],
        barThickness: 12,
      },
      {
        label: "DAY 4",
        data: [12, 12, 8],
        barThickness: 12,
      },
      {
        label: "DAY 5",
        data: [14, 12, 8],
        barThickness: 12,
      },
      {
        label: "DAY 6",
        data: [8, 10, 13],
        barThickness: 12,
      },
      {
        label: "DAY 7",
        data: [12, 8, 20, 0],
        barThickness: 12,
      },
    ],
  },
  options: {
    legend: {
      display: true,
      position: "bottom",
    },
    scales: {
      yAxes: [
        {
          ticks: {
            // stepSize: 100000,
            callback: function (value) {
              var ranges = [
                //  { divider: 1e6, suffix: 'M' },
                { divider: 1e3, suffix: "k" },
              ];
              function formatNumber(n) {
                for (var i = 0; i < ranges.length; i++) {
                  if (n >= ranges[i].divider) {
                    return (
                      (n / ranges[i].divider).toString() + ranges[i].suffix
                    );
                  }
                }
                return n;
              }
              return formatNumber(value) + "k";
            },
          },
        },
      ],
    },
  },
};

/*------------------- waterIntakeData---------------------- */
const wateIntakeData = {
  data: {
    data: [5, 4.5, 4, 3.5, 3, 2.5, 2, 1.5, 1, 0.5, 0],
    labels: [
      "PT W1",
      "PT W2",
      "W1",
      "W2",
      "W3",
      "W4",
      "W5",
      "W6",
      "W7",
      "W8",
      "W9",
      "W10",
      "W11",
      "W12",
      "W13",
    ],
    datasets: [
      {
        label: "DAY 1",
        data: [2.5, 3, 3.8],
        barThickness: 12,
      },
      {
        label: "DAY 2",
        data: [3.5, 3, 3.8],
        barThickness: 12,
      },
      {
        label: "DAY 3",
        data: [2, 3.5, 5],
        barThickness: 12,
      },
      {
        label: "DAY 4",
        data: [3, 3, 3.8],
        barThickness: 12,
      },
      {
        label: "DAY 5",
        data: [3.5, 3, 3.8],
        barThickness: 12,
      },
      {
        label: "DAY 6",
        data: [2, 2.5, 3.3],
        barThickness: 12,
      },
      {
        label: "DAY 7",
        data: [3, 3.5, 4.5, 0],
        barThickness: 12,
        borderColor: "black",
      },
    ],
  },
  options: {
    legend: {
      display: true,
      position: "bottom",
    },
    scales: {
      yAxes: [
        {
          ticks: {
            // stepSize: 100000,
            callback: function (value) {
              var ranges = [
                { divider: 1e6, suffix: "M" },
                { divider: 1e3, suffix: "k" },
              ];
              function formatNumber(n) {
                for (var i = 0; i < ranges.length; i++) {
                  if (n >= ranges[i].divider) {
                    return (
                      (n / ranges[i].divider).toString() + ranges[i].suffix
                    );
                  }
                }
                return n;
              }
              return formatNumber(value) + "L";
            },
          },
        },
      ],
    },
  },
  chartArea: {
    backgroundColor: "white",
  },
};

/*----------------------workoutData--------------------- */
const workOutData = {
  data: {
    data: [100, 90, 80, 70, 60, 50, 40, 30, 20, 10, 0],
    labels: [
      "PT W1",
      "PT W2",
      "W1",
      "W2",
      "W3",
      "W4",
      "W5",
      "W6",
      "W7",
      "W8",
      "W9",
      "W10",
      "W11",
      "W12",
      "W13",
    ],
    datasets: [
      {
        label: "DAY 1",
        data: [50, 60, 40],
        barThickness: 12,
      },
      {
        label: "DAY 2",
        data: [60, 60, 40],
        barThickness: 12,
      },
      {
        label: "DAY 3",
        data: [40, 40, 70],
        barThickness: 12,
      },
      {
        label: "DAY 4",
        data: [60, 60, 40],
        barThickness: 12,
      },
      {
        label: "DAY 5",
        data: [70, 60, 40],
        barThickness: 12,
      },
      {
        label: "DAY 6",
        data: [40, 50, 100],
        barThickness: 12,
      },
      {
        label: "DAY 7",
        data: [60, 40, 30, 0],
        barThickness: 12,
      },
    ],
  },
  options: {
    legend: {
      display: true,
      position: "bottom",
    },
    scales: {
      yAxes: [
        {
          ticks: {
            callback: function (value) {
              var ranges = [
                { divider: 1e6, suffix: "M" },
                { divider: 1e3, suffix: "k" },
              ];
              function formatNumber(n) {
                for (var i = 0; i < ranges.length; i++) {
                  if (n >= ranges[i].divider) {
                    return (
                      (n / ranges[i].divider).toString() + ranges[i].suffix
                    );
                  }
                }
                return n;
              }
              return formatNumber(value) + "%";
            },
          },
        },
      ],
    },
  },
};
/*------------------------------------------------- */
const MealtrackerData = [
  {
    name: "PT W1",
    Breakfast: 4000,
    Morning: 2400,
    Lunch: 2400,
    Afternoon: 3000,
    Evening: 3000,
    Dinner: 3000,
  },
  {
    name: "",
    Breakfast: 4000,
    Morning: 2400,
    Lunch: 2400,
    Afternoon: 3000,
    Evening: 3000,
    Dinner: 3000,
  },
  {
    name: "",
    Breakfast: 4000,
    Morning: 2400,
    Lunch: 2400,
    Afternoon: 3000,
    Evening: 3000,
    Dinner: 3000,
  },
  {
    name: "",
    Breakfast: 4000,
    Morning: 2400,
    Lunch: 2400,
    Afternoon: 3000,
    Evening: 3000,
    Dinner: 3000,
  },
  {
    name: "",
    Breakfast: 4000,
    Morning: 2400,
    Lunch: 2400,
    Afternoon: 3000,
    Evening: 3000,
    Dinner: 3000,
  },
  {
    name: "",
    Breakfast: 4000,
    Morning: 2400,
    Lunch: 2400,
    Afternoon: 3000,
    Evening: 3000,
    Dinner: 3000,
  },
  {
    name: "",
    Breakfast: 4000,
    Morning: 2400,
    Lunch: 2400,
    Afternoon: 3000,
    Evening: 3000,
    Dinner: 3000,
  },
  {
    name: "PT W2",
    Breakfast: 0,
    Morning: 0,
    Lunch: 0,
    Afternoon: 0,
    Evening: 0,
    Dinner: 0,
  },
  {
    name: "W01",
    Breakfast: 0,
    Morning: 0,
    Lunch: 0,
    Afternoon: 0,
    Evening: 0,
    Dinner: 0,
  },
  {
    name: "W02",
    Breakfast: 0,
    Morning: 0,
    Lunch: 0,
    Afternoon: 0,
    Evening: 0,
    Dinner: 0,
  },
  {
    name: "W03",
    Breakfast: 0,
    Morning: 0,
    Lunch: 0,
    Afternoon: 0,
    Evening: 0,
    Dinner: 0,
  },
  {
    name: "W04",
    Breakfast: 0,
    Morning: 0,
    Lunch: 0,
    Afternoon: 0,
    Evening: 0,
    Dinner: 0,
  },
  {
    name: "w05",
    Breakfast: 0,
    Morning: 0,
    Lunch: 0,
    Afternoon: 0,
    Evening: 0,
    Dinner: 0,
  },
  {
    name: "w06",
    Breakfast: 0,
    Morning: 0,
    Lunch: 0,
    Afternoon: 0,
    Evening: 0,
    Dinner: 0,
  },
  {
    name: "w07",
    Breakfast: 0,
    Morning: 0,
    Lunch: 0,
    Afternoon: 0,
    Evening: 0,
    Dinner: 0,
  },
  {
    name: "w08",
    Breakfast: 0,
    Morning: 0,
    Lunch: 0,
    Afternoon: 0,
    Dinner: 0,
  },
  {
    name: "w09",
    Breakfast: 0,
    Morning: 0,
    Lunch: 0,
    Afternoon: 0,
    Evening: 0,
    Dinner: 0,
  },
  {
    name: "w10",
    Breakfast: 0,
    Morning: 0,
    Lunch: 0,
    Afternoon: 0,
    Evening: 0,
    Dinner: 0,
  },
  {
    name: "w11",
    Breakfast: 0,
    Morning: 0,
    Lunch: 0,
    Afternoon: 0,
    Evening: 0,
    Dinner: 0,
  },
  {
    name: "w12",
    Breakfast: 0,
    Morning: 0,
    Lunch: 0,
    Afternoon: 0,
    Evening: 0,
    Dinner: 0,
  },
  {
    name: "w13",
    Breakfast: 0,
    Morning: 0,
    Lunch: 0,
    Afternoon: 0,
    Evening: 0,
    Dinner: 0,
  },
  {
    name: "w14",
    Breakfast: 0,
    Morning: 0,
    Lunch: 0,
    Afternoon: 0,
    Evening: 0,
    Dinner: 0,
  },
  {
    name: "w15",
    Breakfast: 0,
    Morning: 0,
    Lunch: 0,
    Afternoon: 0,
    Evening: 0,
    Dinner: 0,
  },
];

const CustomTooltip = ({ active, payload, label }) => {
  if (payload) {
    return <div className="custom-tooltip"></div>;
  }

  return null;
};

export default function ViewDetails(props) {
  const [show, setShow] = useState(false);
  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);
  const [formInputs, setFormInputs] = useState();
  const [key, setKey] = useState("1");
  const [stepCount, setStepCount] = useState();
  const [waterIntake, setWaterIntake] = useState();

  //Line chart

  const [options, setOptions] = useState({
    responsive: true,
    legend: {
      display: true,
      position: "bottom",
    },
    scales: {
      yAxes: {
        type: "linear",
        display: true,
        title: {
          display: true,
          text: "y axis",
        },
        ticks: {
          suggestedMin: 35,
          suggestedMax: 200,
        },
      },
      xAxes: [
        {
          ticks: {
            stepSize: 1,
            beginAtZero: true,
            autoSkip: false,
            maxRotation: 0,
            minRotation: 0,
          },
        },
      ],
    },
    type: "line",
  });

  //Measurements-Data
  const measurments = {
    labels: ["PT W1", "PT W2", "W1", "W2", "W3", "W4", "W5", "W6", "W7"],
    data: [10, 15, 20, 25, 30, 35, 40, 45, 50, 55, 60],
    datasets: [
      {
        label: "cheast",
        fill: false,
        data: [1, 2, 3, 4, 5, 6, 7, 8, 90],
        borderColor: ["red"],
        borderWidth: 1,
        pointBackgroundColor: "red",
      },
      {
        label: "waist",
        fill: false,
        data: [4, 5, 6, 7, 8],
        borderColor: ["blue"],
        borderWidth: 1,
        pointBackgroundColor: "blue",
      },
      {
        label: "arm",
        fill: false,
        lineThickness: 10,
        data: [6, 7, 8, 9, 10],
        borderColor: ["yellow"],
        borderWidth: 1,
        pointBackgroundColor: "yellow",
      },
      {
        label: "leg",
        fill: false,
        data: [25, 30, 15],
        borderColor: ["green"],
        borderWidth: 1,
        pointBackgroundColor: "green",
      },
      {
        label: "hip",
        fill: false,
        data: [10, 25, 5],
        borderColor: ["pink"],
        borderWidth: 1,
        pointBackgroundColor: "pink",
      },
    ],
  };

  //Weight-Data
  const weight = {
    labels: ["PT W1", "PT W2", "W1", "W2", "W3", "W4", "W5", "W6", "W7"],
    data: [45, 50, 55, 60, 65, 70, 75, 80, 85, 90, 95],
    datasets: [
      {
        label: "weight",
        fill: false,
        data: [75, 80, 85, 90],
        borderColor: ["blue"],
        borderWidth: 1,
        pointBackgroundColor: "blue",
      },
    ],
  };

  // const [options, setOptions] = useState("")
  //   responsive: true,
  //   legend: {
  //     display: true,
  //     position: "bottom",
  //   },
  //   scales: {
  //     yAxes: {
  //       type: "linear",
  //       display: true,
  //       title: {
  //         display: true,
  //         text: "y axis",
  //       },
  //       ticks: {
  //         suggestedMin: 35,
  //         suggestedMax: 200,
  //       },
  //     },
  //     xAxes: [
  //       {
  //         ticks: {
  //           stepSize: 1,
  //           beginAtZero: true,
  //           autoSkip: false,
  //           maxRotation: 0,
  //           minRotation: 0,
  //         },
  //       },
  //     ],
  //   },
  //   type: "line",
  // });

  return (
    <>
      <Modal isOpen={true}>
        <Row>
          <Col className="modal-navbar">
            <div className="">
              <RiCloseFill
                size={35}
                className="close"
                onClick={() => props.close(false)}
              />
            </div>
            <div>
              <Tabs
                id="progress-tracker-tab"
                activeKey={key}
                onSelect={(k) => setKey(k)}
              >
                <Tab eventKey="1" title="Water Intake" className="intake ">
                  <div className="work p-3">
                    <lable className="program-lable ">Water Intake</lable>
                    <div className="bg-white">
                      <Bar
                        className="bar"
                        data={wateIntakeData.data}
                        options={wateIntakeData.options}
                        chartArea={wateIntakeData.chartArea}
                      ></Bar>
                    </div>
                  </div>
                </Tab>
                <Tab eventKey="2" title="Steps Count" className="intake">
                  <div className="work p-3">
                    <lable className="program-lable">Steps Count</lable>
                    <div className="bg-white points">
                      <Bar
                        className="bar"
                        data={stepCountData.data}
                        options={stepCountData.options}
                      ></Bar>
                    </div>
                  </div>
                </Tab>
                <Tab eventKey="3" title="Workout %" className="intake">
                  <div className="work p-3">
                    <lable className="program-lable">Workout Rounds</lable>
                    <div className="bg-white points">
                      <Bar
                        className="bar"
                        data={workOutData.data}
                        options={workOutData.options}
                      ></Bar>
                    </div>
                  </div>
                </Tab>
                <Tab eventKey="4" title="Diet Plan">
                  <div className="diet-plan">
                    <h4 className="ml-3 pt-2 mb-4"> Diet plan </h4>
                    <div class="row ml-3 mb-4">
                      <div class="col-0 pr-5">
                        <b> Diet: Vegan </b>
                      </div>
                      <div class="col-0">
                        <b> Plan Name: Program 2 Vegan </b>
                      </div>
                      <div class="col-0  pl-5">
                        <b> Week: 7 Week </b>
                      </div>
                      <div class="col-7  pl-6 ml-3 d-flex justify-content-end drop-down ">
                        <div class="dropdown ">
                          <button
                            class=" dropdown-toggle"
                            type="button"
                            id="dropdownMenuButton"
                            data-toggle="dropdown"
                            aria-haspopup="true"
                            aria-expanded="false"
                          >
                            PT W1
                          </button>
                          <div
                            class="dropdown-menu"
                            aria-labelledby="dropdownMenuButton"
                          >
                            <a class="dropdown-item" href="#">
                              Action
                            </a>
                            <a class="dropdown-item" href="#">
                              Another action
                            </a>
                            <a class="dropdown-item" href="#">
                              Something else here
                            </a>
                          </div>
                        </div>
                      </div>
                    </div>
                    <ModalPopDietpanelMenu />
                  </div>
                </Tab>
                <Tab eventKey="5" title="Progress tracker" className="intake">
                  <div className="work p-3">
                    <div className=" bg-white ml-5" style={{ maxWidth: "90%" }}>
                      <Tabs>
                        <Tab eventKey="1" title="Measurements">
                          <LineChart data={measurments} options={options} />
                        </Tab>
                        <Tab eventKey="2" title="Weights">
                          <LineChart data={weight} options={options} />
                        </Tab>
                      </Tabs>

                      {/* <ProgressTrackerBody user_id={[props.user_id, ""]} /> */}
                    </div>
                  </div>

                  {/* <ViewDetails /> */}
                  {/* <div className="main-content">
                                            <p className="nav-one-heading">
                                                <b>process tracker-PT Week 2- Day 5</b>
                                            </p>
                                            <div class="dropdown button-one">
                                                <button class="btn btn-light dropdown-toggle " type="button" data-toggle="dropdown">PT W1
                                                    <span class="caret"></span></button>
                                                <ul class="dropdown-menu ">
                                                    <li><a href="#">HTML</a></li>
                                                    <li><a href="#">CSS</a></li>
                                                    <li><a href="#">JavaScript</a></li>
                                                </ul>
                                            </div>

                                            <div className="main-chart">
                                                <div className="chart-one">
                                                    <Tabs>
                                                        <Tab eventKey="1" title="Measurements">
                                                            <LineChart data={measurments} options={options} />
                                                        </Tab>
                                                        <Tab eventKey="2" title="Weights">
                                                            <LineChart data={weight} options={options} />
                                                        </Tab>
                                                    </Tabs>
                                                </div>
                                            </div> */}

                  {/* <div className="six-profile-img">
                                                <div className="set-1">
                                                    <div className="fv-box-1">
                                                        <img className="profile_img_one" src={"#"} />
                                                    </div>
                                                    <div className="fv-box-1">
                                                        <img className="profile_img_one" src={"#"} />
                                                    </div>
                                                   
                                                </div>

                                                <div className="set-2">
                                                    <div className="fv-box-2">
                                                        <img className="profile_img_one" src={"#"} />
                                                    </div>
                                                    <div className="fv-box-2">
                                                        <img className="profile_img_one" src={"#"} />
                                                    </div>
                                                   
                                                </div> */}

                  {/* <div className="set-3">
                                                    <div className="fv-box-3">
                                                        <img className="profile_img_one" src={"#"} />
                                                    </div>
                                                    <div className="fv-box-3">
                                                        <img className="profile_img_one" src={"#"} />
                                                    </div>
                                                    
                                                </div> */}
                  {/* </div> */}

                  {/* <div className="open-img-gallery">
                    <button type="button" classbtn btn-link>
                      <u>
                        <b>Open image gallery</b>
                      </u>
                    </button>
                  </div> */}

                  {/* </div> */}
                </Tab>
                <Tab eventKey="6" title="Meal tracker" className="intake">
                  <div className="work p-3">
                    <lable className="program-lable">Workout Rounds</lable>
                    <div className="bg-white points">
                      <BarChart
                        width={1350}
                        height={700}
                        data={MealtrackerData}
                        margin={{
                          top: 30,
                          right: 10,
                          left: 20,
                          bottom: 20,
                        }}
                        // barCategoryGap={10}
                        barSize={5}
                      >
                        <CartesianGrid strokeDasharray="3 3" />
                        <XAxis dataKey="name" />
                        <YAxis />
                        <Tooltip content={<CustomTooltip />} />
                        <Legend />
                        <Recharts
                          dataKey="Breakfast"
                          stackId="a"
                          fill="#0D08FF"
                        />
                        <Recharts
                          dataKey="Morning"
                          stackId="a"
                          fill="#FCB12B"
                        />
                        <Recharts dataKey="Lunch" stackId="a" fill="#D25151" />
                        <Recharts
                          dataKey="Afternoon"
                          stackId="a"
                          fill="#6AFD37"
                        />
                        <Recharts
                          dataKey="Evening"
                          stackId="a"
                          fill="#FF57EE"
                        />
                        <Recharts dataKey="Dinner" stackId="a" fill="#393939" />

                        {/* <Recharts dataKey="qw" stackId="b" fill="#0D08FF" />
                        <Recharts dataKey="ww" stackId="b" fill="#FCB12B" />
                        <Recharts dataKey="ew" stackId="b" fill="#D25151" />
                        <Recharts dataKey="rw" stackId="b" fill="#6AFD37" />
                        
                        

                        <Recharts dataKey="qw" stackId="c" fill="#0D08FF" />
                        <Recharts dataKey="ww" stackId="c" fill="#FCB12B" />
                        <Recharts dataKey="ew" stackId="c" fill="#D25151" />
                        <Recharts dataKey="rw" stackId="c" fill="#6AFD37" />
                        <Recharts dataKey="tw" stackId="c" fill="#FF57EE" />
                        <Recharts dataKey="yw" stackId="c" fill="#393939" />

                        <Recharts dataKey="qw" stackId="d" fill="#0D08FF" />
                        <Recharts dataKey="ww" stackId="d" fill="#FCB12B" />
                        <Recharts dataKey="ew" stackId="d" fill="#D25151" />
                        <Recharts dataKey="rw" stackId="d" fill="#6AFD37" />
                        <Recharts dataKey="tw" stackId="d" fill="#FF57EE" />
                        

                        <Recharts dataKey="qw" stackId="e" fill="#0D08FF" />
                        <Recharts dataKey="ww" stackId="e" fill="#FCB12B" />
                        <Recharts dataKey="ew" stackId="e" fill="#D25151" />
                        <Recharts dataKey="rw" stackId="e" fill="#6AFD37" />
                        

                        <Recharts dataKey="qw" stackId="f" fill="#0D08FF" />
                        <Recharts dataKey="ww" stackId="f" fill="#FCB12B" />
                        <Recharts dataKey="ew" stackId="f" fill="#D25151" />
                        <Recharts dataKey="rw" stackId="f" fill="#6AFD37" />
                        <Recharts dataKey="tw" stackId="f" fill="#FF57EE" />
                        <Recharts dataKey="yw" stackId="f" fill="#393939" />

                        <Recharts dataKey="qw" stackId="g" fill="#0D08FF" />
                        <Recharts dataKey="ww" stackId="g" fill="#FCB12B" />
                        <Recharts dataKey="ew" stackId="g" fill="#D25151" />
                        <Recharts dataKey="rw" stackId="g" fill="#6AFD37" />
                        <Recharts dataKey="tw" stackId="g" fill="#FF57EE" /> */}
                      </BarChart>
                    </div>
                  </div>
                </Tab>
              </Tabs>
            </div>
          </Col>
        </Row>
      </Modal>
    </>
  );
}
