import React, { Component } from "react";
import { Col, Row, Button, Container, Form , InputGroup, FormControl} from "react-bootstrap";
import Pusher from "pusher-js";
import Linkify from 'react-linkify';
import { AiOutlineCamera } from 'react-icons/ai';
// import { useLocation } from "react-router-dom";
// import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import { postMethod, copyObjs, tokenError,postMethodFormData } from "../Library";
import ModalPop from "../components/modals";

import { confirmAlert } from "react-confirm-alert";
import "react-confirm-alert/src/react-confirm-alert.css";

import PlaceholderImage from "../assets/images/blackUser.png";
import { set, update } from "lodash";
import moment from "moment";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faTrash } from "@fortawesome/free-solid-svg-icons";
import { BsThreeDotsVertical } from "react-icons/bs";

import { pusherKey } from "../Library/configs";
import ImageBoxUpload from '../components/ImageBox/imageboxupload'
export default class chatData extends Component {
  constructor(props) {
    super(props);

    this.state = {
      msg: "",
      type: "",
      fromDateChange: "",
      toDate: "",
      fromDate: "",
      day_by: "",
      filterPillType: "",
      filterDateVal: "",
      msgData: [],
      show: false,
      dateMsg: "",
      id: "",
      pusher_channel: null,
      pusher_name: null,
      showToast: false,
      toastMessage: "",
      selectedImg:null,
      imgLoading:false,
      imageUrl:null,
      showPreview:false,
      userNamePro:'',
      ValueInput:''

    };
    this.msgBoxRef = React.createRef();
    this.inputChatBox = React.createRef();
  }

  componentDidMount() {
    console.log(this.props.user_name)
    this.getFilterMsg();
    if(this.props.user_name.length>12){
let name = this.props.user_name.slice(0, 10) + '...';
this.setState({userNamePro:name})
    }
    // console.log("props user", this.props);
    const { msgData } = this.state;
    if (this.props.id) {
      this.setState(
        {
          id: this.props.id,
        },
        () => {
          this.getMsgData();
          const id = localStorage.getItem("user");
          const UserId = id ? JSON.parse(id).id : "";
          const pusher = new Pusher(pusherKey, {
            cluster: "ap2",
            encrypted: true,
          });
          // console.log(pusher);
          const channel = pusher.subscribe(`ch-${UserId}`);
          // console.log("channel", msgData);
          this.setState({
            pusher_channel: channel,
            pusher_name: pusher,
          });
          channel.bind("chat-data", (data) => {
            console.log(data,'data')
            const newArr = copyObjs(this.state.msgData);
            if(data.is_deleted=="1"){
              this.getMsgData(this.state.id)

            }
            if (data.chat_history) {
              const updateId = parseInt(data.value.sender_id);
              // console.log(
              //   "inside cht",
              //   updateId,
              //   parseInt(this.state.id),
              //   this.state.msgData,
              //   newArr
              // );
              if (updateId === parseInt(this.state.id)) {
                // const newMsgs = msgData
                newArr.unshift(data.value);
                console.log(data.value,"value") 
                // console.log("inside cht updating", msgData, newArr);
                this.setState({
                  msgData: newArr,
                });

                this.toUpdateSeen(this.state.id);
              }
            } 
            // console.log("notificatrion bind", data, msgData);
          });
        }
      );
    }
  }

  componentWillUnmount() {
    if (this.state.pusher_name && this.state.pusher_channel) {
      this.state.pusher_name.unsubscribe(this.state.pusher_channel);
      this.state.pusher_channel.unbind();
      // console.log("pusher unsubsribed chat history");
    }
  }

  showToastFn = (msg) => {
    this.setState({
      showToast: true,
    });
    if (typeof msg == "string") {
      this.setState({
        toastMessage: msg,
      });
    } else {
      this.setState({
        toastMessage: JSON.stringify(msg, null, 2),
      });
    }
  };

  toUpdateSeen = async (id) => {
    const data = {
      url: "chat_history_status_update_by_nutrition",
      body: {
        sender_id: id,
      },
    };
    const newData = await postMethod(data);
    this.clearFilters();
    this.focusLastMsg();

    // refresh user list
  };

  static getDerivedStateFromProps(nextProps, prevState) {
    const currentSlug = nextProps.id;
    const oldSlug = prevState.id;
    if (currentSlug !== oldSlug) {
      return { id: currentSlug };
    } else {
      return null;
    }
  }

  componentDidUpdate(prevProps, prevState) {
    console.log(prevState,'prevState')
    if (prevState.id !== this.state.id) {
      this.getFilterMsg()
      if(this.props.user_name.length>12){
        let name = this.props.user_name.slice(0, 10) + '...';
        this.setState({userNamePro:name})
            }
      // this.getMsgData();
    }
  }
//   onImageChange=(e)=>{
// this.setState({
//   selectedImg:e.target.files[0]
// })
// let fileData = [];
//     for (let i = 0; i < e.target.files.length; i++) {
//         fileData.push(e.target.files[i]);
//     }
// this.uploadImageFn(fileData)

//   }
//   onImageChange = async (event) => {
//     this.setState({
//         imgLoading: true
//     })
//     let fileData = [];
//     for (let i = 0; i < event.target.files.length; i++) {
//         fileData.push(event.target.files[i]);
//     }
//     const data = {
//         url: 'image_upload',
//         body: { image: fileData }
//     }
//     const imgUpload = await imagePost(data)
//     if (imgUpload.status == 201) {
//         this.setState({
//             imgLoading: false,
//             obj: { ...this.state.obj, image: imgUrl + imgUpload.data.image }
//         }, () => {
//             console.log('after uploading', this.state)
//         })
//     }
// }

  textChange = (e) => {
    const { name, value } = e.target;
    this.setState({
      msg: value,
    });
    let el = this.inputChatBox.current;
    el.style.cssText = "height: min-content";
    el.style.cssText = "height:" + el.scrollHeight + "px";
  };

  enterMsg = async (e) => {
    const { msg } = this.state;
    e.preventDefault();
    if (msg.length > 0 || this.state.imgUrl!==null) {
      const data = {
        url: "add_nutritionist_chat",
        body: {
          message: this.state.imgUrl ? this.state.imgUrl : msg ,
          sender_id: this.state.id,
          // type: this.state.selectedImg!==null?"image":"single-chat"
          type: this.state.imgUrl? "image" : "single-chat"
        },
      };
      const newData = await postMethod(data);
      if (newData.status === 1) {
        this.setState({
          msg: "",
        });
        this.inputChatBox.current.style.cssText = "height: min-content";
        this.getMsgData();
        this.focusLastMsg();
        this.closePreview();
      } else if (newData.status === false) {
        tokenError(newData.error);
      } else {
        this.showToastFn(newData?.message ? newData?.message : newData);
      }
    }
  };

  focusLastMsg = () => {
    const msBx = this.msgBoxRef.current;
    msBx.scrollTop = msBx.scrollHeight;
  };

  clearFilters = () => {
    // this.props.searchBoxRef.current.value = "";
    // this.setState({
    //   day_by: "",
    //   fromDate: "",
    //   toDate: "",
    //   type: "",
    //   filterDateVal: "",
    //   ValueInput:""

    // });
    // filterPillType: "",
  };

  getMsgData = async () => {
    // console.log("getting msg Data");
    this.clearFilters();
    const data = {
      url: "nutritionist_chat_history_list",
      body: {
        sender_id: this.state.id,
      },
    };
    const newData = await postMethod(data);
    if (newData.status === 1) {
      // console.log("got msg Data");
      this.setState({
        msgData: newData.data.reverse(),
      });
      // get user list
    } else if (newData.status === false) {
      tokenError(newData.error);
    } else {
      this.setState({
        msgData: [],
      });
    }
  };

  getFilterMsg = async (a) => {
    const data = {
      url: "nutritionist_chat_history_list",
      body: {
        sender_id: this.state.id,
        day_by: this.state.day_by,
        from_date: this.state.fromDate,
        to_date: this.state.toDate,
        type: this.state.type,
        search: { value: this.state.ValueInput },
      },
    };

    const newData = await postMethod(data);
    if (newData.status === 1) {
      this.setState({
        msgData: newData.data.length > 0 ? newData.data.reverse() : [],
      });
      // for custom date filter
      if (this.state.fromDate && this.state.toDate) {
        if (newData.data.length > 0) {
          this.setState({
            show: false,
          });
        } else {
          this.setState({
            dateMsg: newData.data.length == 0 ? newData.message : "",
          });
        }
      }
    } else if (newData.status === false) {
      tokenError(newData.error);
    } else {
      this.setState({
        dateMsg: newData.message,
      });
    }
  };


  uploadImageFn = async (e) => {
    if (e.target.files.length > 0) {
      // setErr();
      // setUploading(true);
      this.setState({
        imgLoading:true,
        selectedImg:e.target.value
      })
      let formDataBody = new FormData();
      formDataBody.append("image_url", e.target.files[0]);
      const data = {
        url: "image_upload",
        body: formDataBody,
      };
      // if (props?.width && props?.height && props.folder) {
      //   data.url = "nws_image_upload";
      //   formDataBody.append("folder_name", props.folder);
      //   formDataBody.append("img_w", props.width);
      //   formDataBody.append("img_h", props.height);
      //   data.body = formDataBody;
      // }
      const newData = await postMethodFormData(data);
      this.setState({
        imgLoading:false,
        // selectedImg:null
      })
      if (newData.status == 1) {
        let url = newData.data.image_url;
        if (url && newData.data){
          this.setState({imgUrl:url,
          })
        // props.data(url, props);
        }
        // setErr();
        this.setState({
          showPreview:true
        })
      } else {
        // setErr(JSON.stringify(newData.message));
      }
    }
  };


  // uploadImageFn = async (e) => {
  //   if (this.state.selectedImg) {
  //     const fd = new FormData();
  //     fd.append("image_url", this.state.selectedImg);
  //     console.log(fd)
  //     let fileData = [];
  //     for (let i = 0; i < event.target.files.length; i++) {
  //         fileData.push(event.target.files[i]);
  //     }
  //     const data = {
  //       url: "image_upload",
  //       body: fd,
  //     };
  //   console.log(this.state.selectedImg.name,'img')
  //     const newData = await postMethod(data);
  //     this.setState({selectedImg:null})
  //     if (newData.status == 1) {
  //     console.log(data)
  //     } else {
  //       // setErr(JSON.stringify(newData.message));
  //     }
  //   }
  // };

  filterChange = (e) => {
    const { name, value } = e.target;
    // console.log("date", value);
    if (name == "filterChatType") {
      this.setState(
        {
          type: value,
        },
        () => {
          this.getFilterMsg();
        }
      );
    } else if (name == "filterDate") {
      this.setState({
        filterDateVal: value,
      });
      if (value === "custom_date") {
        this.setState({
          show: true,
        });
      } else {
        this.setState(
          {
            day_by: value,
            fromDate: "",
            toDate: "",
          },
          () => {
            this.getFilterMsg();
          }
        );
      }
    }
  };

  handleSearchInput=(e)=>{
this.setState({ValueInput:e.target.value},
  () => {
    this.getFilterMsg();
  }
  )

  }

  // filterPillChange = (e) => {
  //   const value = e.target.getAttribute("dataid");
  //   this.setState(
  //     {
  //       filterPillType: value == this.state.filterPillType ? "" : value,
  //     },
  //     () => {
  //       this.getFilterMsg(value);
  //     }
  //   );
  // };

  fromDateChange = (e) => {
    const { name, value } = e.target;
    this.setState({
      fromDate: value,
    });
  };

  deleteChat = (e) => {
    e.preventDefault();
    const id = e.target.getAttribute("dataid");
    confirmAlert({
      title: "Confirm delete this message ?",
      message: "Note: This will delete the message permanently.",
      buttons: [
        {
          label: "Ok",
          onClick: async () => {
            const data = {
              url: "delete_nutritionist_chat",
              body: {
                chat_id: parseInt(id),
              },
            };
            const newData = await postMethod(data);
            this.showToastFn(newData.message);
            if (newData.status == 1) {
              this.getMsgData();
            }
          },
        },
        {
          label: "Cancel",
          onClick: () => console.log("Cancel Delete"),
        },
      ],
      closeOnEscape: false,
      closeOnClickOutside: false,
    });
  };

  toDateChange = (e) => {
    this.setState({
      toDate: e.target.value,
    });
  };
  closePreview=()=>{
    this.setState({
      showPreview:false,
      imgUrl:null
    })
  }
  closeModal = () => {
    this.setState(
      {
        show: false,
        day_by: "",
        fromDate: "",
        toDate: "",
        filterDateVal: "",
      },
      () => {
        if (this.state.dateMsg.length > 0) {
          this.getFilterMsg();
          this.setState({
            dateMsg: "",
          });
        }
      }
    );
  };

  minTwoDigit = (n) => {
    return (n < 10 ? "0" : "") + n;
  };

  dateFormatter = (cell) => {
    let date = new Date(`${cell}`);
    let year = date.getFullYear();
    let month = this.minTwoDigit(date.getMonth() + 1);
    let day = this.minTwoDigit(date.getDate());
    function formatAMPM(date) {
      let hours = date.getHours();
      let minutes = date.getMinutes();
      let ampm = hours >= 12 ? "PM" : "AM";
      hours = hours % 12;
      hours = hours ? hours : 12; // the hour '0' should be '12'
      minutes = minutes < 10 ? "0" + minutes : minutes;
      let strTime = hours + ":" + minutes + " " + ampm;
      return strTime;
    }
    return (
      <>
        <span className="chattimebx">
          {`${year}-${month}-${day} `}
          {formatAMPM(date)}
        </span>
      </>
    );
  };

  getFirstLetter = (name) => {
    let x = name.split("");
    return x[0];
  };
modalPreview=()=>{
  return(
    <div >
      <Row>
        <Col>
        <div className=" d-flex align-items-center justify-content-center mwx-400 ml-auto mr-auto">
    {this.state.imgUrl && <img src={this.state.imgUrl} alt="image" className="img-chat" />}
    </div>
    </Col></Row>
    <Row>
    <Col>
    <div className="d-flex mt-4 float-right">
          <Button
            variant="primary"
            className="mr-3"
            // disabled={uploading}
            onClick={this.closePreview}
          >
            Cancel
          </Button>
          <Button
            variant="primary"
            className="mr-3"
            // disabled={uploading}
            onClick={(e) => this.enterMsg(e)}
          >
            Upload
          </Button>
          </div>
          </Col>
          </Row>
    </div>
  )
}
  modalBody = () => {
    return (
      <div className="p-3">
        <div className="d-flex align-items-center">
          <Form.Group className="w-100 mb-0 mx-3 mwx-300" controlId="email">
            <Form.Label>From</Form.Label>
            <Form.Control
              type="date"
              name="from_date"
              size="lg"
              className="mb-0"
              onChange={this.fromDateChange}
              placeholder="Search"
            />
          </Form.Group>
          <Form.Group className="w-100 mb-0 mx-3 mwx-300" controlId="email">
            <Form.Label>To</Form.Label>
            <Form.Control
              type="date"
              name="to_date"
              size="lg"
              onChange={this.toDateChange}
              className="mb-0 ml-0"
              placeholder="Search"
            />
          </Form.Group>
        </div>

        {this.state.dateMsg && (
          <h4 className="text-left pl-3 mt-2 mb-0 text-capitalize err-feedback text-big">
            Oops!! {this.state.dateMsg}. Please try your search with a different
            date.
          </h4>
        )}

        <br />
        <Row className="ml-3">
          <Button
            onClick={this.closeModal}
            className="mt-2"
            variant="secondary"
          >
            Cancel
          </Button>
          <Button
            onClick={() => {
              this.setState(
                {
                  day_by: "",
                },
                () => {
                  this.getFilterMsg();
                }
              );
            }}
            disabled={!this.state.fromDate || !this.state.toDate}
            className="mt-2 ml-3"
            variant="primary"
          >
            Apply
          </Button>
        </Row>
      </div>
    );
  };

  render() {
    return (
      <>
        {/* <div className="m-3 h-100 d-flex flex-column justify-content-between"> */}
        <div className="headerBox">
          <div className="d-flex align-items-center chat_total_height">
            {/* <div className="actionPills">
              <span
                className={`pill mr-2 ${
                  this.state.filterPillType == "feedback" ? "active" : ""
                }`}
                dataid="feedback"
                onClick={this.filterPillChange}
              >
                Feedback
              </span>
              <span
                className={`pill mr-2 ${
                  this.state.filterPillType == "meal-tracker" ? "active" : ""
                }`}
                dataid="meal-tracker"
                onClick={this.filterPillChange}
              >
                Meal Tracker
              </span>
              <span
                className={`pill mr-2 ${
                  this.state.filterPillType == "progress-tracker"
                    ? "active"
                    : ""
                }`}
                dataid="progress-tracker"
                onClick={this.filterPillChange}
              >
                Progress Tracker
              </span>
              <span
                className={`pill ${
                  this.state.filterPillType == "habit" ? "active" : ""
                }`}
                dataid="habit"
                onClick={this.filterPillChange}
              >
                Habit
              </span>
            </div> */}

            <div className="user-profile d-flex align-items-center justify-content-between">
              <div className="d-flex pl-2 align-items-center chat_logo_name">
                <img
                  src={
                    this.props.user_image_url
                      ? this.props.user_image_url
                      : PlaceholderImage
                  }
                  className="profile-pic"
                />
                <div className="ml-2">
                  {this.props.user_name.length<13 ?
                  <p className="mb-0">
                    <b>{this.props.user_name}</b>
                  </p>:
                  <p className="mb-0">
                  <b>{this.state.userNamePro}</b>
                </p> 
                  }
                </div>
              </div>
              {/* {this.props.filter ? () : null} */}
            </div>

            <div className="ml-auto d-flex align-items-center">
              {/* <h6 className="mr-3 mb-0">Filter by</h6> */}
          
                {/* <Form.Label className="required">Filter By Date:</Form.Label> */}
          {/* <Form.Label className="label_chat_msg_by">Filter Msg by</Form.Label> */}
                <Form.Control
                  as="select"
                  name="filterChatType"
                  className="chat_filter_msg_by bg_2"
                  value={this.state.type}
                  onChange={this.filterChange}
                >
                  <option value="">All</option>
                  <option value="feedback">Feedback</option>
                  <option value="progress-tracker">Progress Tracker</option>
                  <option value="meal-tracker">Meal Tracker</option>
                  <option value="habit">Habit</option>
                </Form.Control>

                {/* <Form.Label className="label_chat_msg_by">Filter by</Form.Label> */}
                <Form.Control
                  as="select"
                  name="filterDate"
                  value={this.state.filterDateVal}
                  onChange={this.filterChange}
                  className="chat_filter_msg_by bg_2"
                  
                >
                  <option value=""> Date</option>
                  <option value="lastday">Last Day</option>
                  <option value="lastweek">Last Week</option>
                  <option value="lastmonth">Last Month</option>
                  <option value="custom_date">Custom date</option>
                </Form.Control>
              
            </div>
            
          </div>
          <div className="d-flex chat_search_head ">
          <Form.Label className="label_chat_msg_by">Search Msg by</Form.Label>
          <InputGroup className="chat-search bg_2">
            <FormControl
              placeholder="Search"
              aria-label="search"
              aria-describedby="basic-addon1"
              className="bg_2 chat_search"
              onChange={this.handleSearchInput}
            />
          </InputGroup>
        </div>
          {/* <hr className="mb-0" /> */}
        </div>

        <div className="messaging position-relative h-100 my-3">
          <div
            className="msg_history bg_2 customScrollBar position-absolute"
            ref={this.msgBoxRef}
            id="content"
          >
            {/* Senders Message */}
            {this.state.msgData.length > 0
              ? this.state.msgData.map((x, i) => {
                  if (x.sender_by === "nutritionist") {
                    return (
                      <div className="d-flex justify-content-end mb-3" key={i}>
                        <div className="msg_box msg_box_send">
                          <div className="msgInb">
                        {x.type=='image'?
                        <img src={x.message}/>:
                       
                            <span className="font-md font-weight-bold mb-0">
                              <Linkify>
                              {x.message}
                              </Linkify>
                            </span> }
                            {/* <img src={}/> */}
                            <p className="font-12 text-muted mt-1 mb-0">
                              <span className="chattimebx">
                                {moment(x.created_at).format("LLL")}
                              </span>
                              <Button
                                variant="link"
                                dataid={x.id}
                                onClick={this.deleteChat}
                                className="delCht p-0 ml-2"
                              >
                                <FontAwesomeIcon icon={faTrash} />
                              </Button>
                            </p>
                          </div>
                          {x.receiver?.image_url ? (
                            <img
                              src={x.receiver.image_url}
                              className="chat-profile-pic ml-2"
                            />
                          ) : (
                            <div className="chatNameBox">
                              {x.receiver?.first_name
                                ? this.getFirstLetter(x.receiver.first_name)
                                : "DEL"}
                            </div>
                          )}
                        </div>
                      </div>
                    );
                  } else {
                    return (
                      <div
                        className="d-flex justify-content-start mb-3"
                        key={i}
                      >
                        <div className="msg_box msg_box_rec">
                          {x.sender?.image_url ? (
                            <img
                              src={x.sender?.image_url}
                              className="chat-profile-pic mr-2"
                            />
                          ) : (
                            <div className="chatNameBox">
                              {x.sender?.first_name
                                ? this.getFirstLetter(x.sender.first_name)
                                : "DEL"}
                            </div>
                          )}

                          <div className="msgInb">
                          
                            <span>
                              {x.type == "feedback" ? (
                                x.feedback.map((feed, index) => (
                                  <div key={index}>
                                    <p className="font-md font-weight-bold text-small mb-0 text-dark">
                                      <i>
                                        <b>Ques</b>{" "}
                                      </i>
                                      : {feed.question}
                                    </p>
                                    <p>
                                      <i>
                                        <b>Ans</b>{" "}
                                      </i>
                                      : {feed.ans}
                                    </p>
                                  </div>
                                ))
                              ) : (
                                <div>
                                {x.type=="image"?
                               <img src={x.message}/>:
                                <span className="font-md font-weight-bold text-small mb-0 text-dark">
                                 <Linkify>
                              {x.message}
                              </Linkify>
                                </span>
                  }
                                </div>
                              )}
                            </span>
                            <p className="font-12 text-muted mt-1 mb-0">
                              <span className="chattimebx">
                                {moment(x.created_at).format("LLL")}
                              </span>
                              
                              {x.type != "single-chat" && x?.type?.length > 0 && (
                                <span
                                  className="typeTag ml-2"
                                  datatype={x.type}
                                  datadate={x.m_pt_date}
                                  dataid={x.sender_id}
                                  imageId={x.message}
                                  disabled={
                                    x.type == "feedback" || x.type == "habit"
                                  }
                                  onClick={(e) => this.props.chooseTag(e)}
                                >
                                  {x.type}
                                </span>
                              )}
                              {/* <span>
                            <BsThreeDotsVertical className="float-right dots"/>
                                </span> */}
                            </p>
                          </div>
                        </div>
                      </div>
                    );
                  }
                })
              : "No messages yet"}
          </div>
          {/* <!-- Typing area --> */}
        </div>
        {/* <form onSubmit={(e) => this.enterMsg(e)}> */}
        <form onSubmit={(e) => e.preventDefault()}>
          <div className=" pl-2 pr-2 type_msg">
            <div className="input_fileds">
              <div className="">
                <div className="input-group bg_2">
                  <textarea
                    rows="1"
                    value={this.state.msg}
                    ref={this.inputChatBox}
                    onChange={(e) => this.textChange(e)}
                    placeholder="Type your message here..."
                    name="message"
                    className="chatTextBox customScrollBar bg_2 form-control rounded-0 border-0 py-0 px-2 "
                  />
                  {/* <input
                    type="text"
                    value={this.state.msg}
                    ref={this.inputChatBox}
                    onChange={(e) => this.textChange(e)}
                    autoComplete="off"
                    placeholder="Type your message here..."
                    name="message"
                    className="form-control rounded-0 border-0 py-4 bg-white"
                  /> */}
          
                 <input style={{display:'none'}} type="file" name="myImage" accept="image/*" onChange={this.uploadImageFn} ref={fileInput=> this.fileInput= fileInput} 
                  onClick={(event)=> { 
                    event.target.value = null
               }} />
                
                 <button style={{border:"none"}} disabled={this.state.msg?true:false} onClick={()=>this.fileInput.click()} >
                 <AiOutlineCamera className="attach-file"/>
                 </button>
                 {/* <ImageBoxUpload/> */}
                  <button
                    type="button"
                    id="button-addon2"
                    tabIndex="-1"
                    className="btn btn-link"
                    onClick={(e) => this.enterMsg(e)}
                  >
                    <svg
                      xmlns="http://www.w3.org/2000/svg"
                      xlink="http://www.w3.org/1999/xlink"
                      aria-hidden="true"
                      focusable="false"
                      width="1em"
                      height="1em"
                      preserveAspectRatio="xMidYMid meet"
                      viewBox="0 0 1792 1792"
                    >
                      <path
                        d="M1764 11q33 24 27 64l-256 1536q-5 29-32 45q-14 8-31 8q-11 0-24-5l-453-185l-242 295q-18 23-49 23q-13 0-22-4q-19-7-30.5-23.5T640 1728v-349l864-1059l-1069 925l-395-162q-37-14-40-55q-2-40 32-59L1696 9q15-9 32-9q20 0 36 11z"
                        fill="#626262"
                      />
                    </svg>
                  </button>
                </div>
              </div>
            </div>
          </div>
        </form>

        <ModalPop
          show={this.state.show}
          onHide={this.closeModal}
          modalcontent={this.modalBody()}
          modalhead={`Custom date`}
        />
          <ModalPop
        show={this.state.showPreview}
        onHide={this.closePreview}
        modalcontent={this.modalPreview()}
        modalhead="Preview Image"
      />
      </>
    );
  }
}
