import React, { useEffect, useState } from "react";
import { Dropdown } from "react-bootstrap";
import Pusher from "pusher-js";

import { postMethodNoLoad, tokenError } from "../../Library";

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faBell } from "@fortawesome/free-solid-svg-icons";
import { Link } from "react-router-dom";
import { pusherKey } from "../../Library/configs";

export default function Index(props) {
  const [notificationCount, setNotificationCount] = useState(0);
  const [notificationList, setNotificationList] = useState([]);
  const [notificationListCount, setNotificationListCount] = useState();

  const stringLength = 50;

  const getNotificationCount = (channel) => {
    // console.log(channel);
    channel.bind("notification-data", (data) => {
      // console.log(data);
      if (data.notification_hearder_count) {
        setNotificationCount(data.value);
        localStorage.setItem("nut_count", data.value);
        getNotificationList();
      }
    });
  };

  const getNotificationList = async () => {
    const data = {
      url: "nutritionist_notification_list",
    };
    const newData = await postMethodNoLoad(data);
    // console.log(newData);
    if (newData.status === 1) {
      setNotificationList(newData.data);
      setNotificationListCount(newData.data.notification_count);
    } else if (newData.status === false) {
      tokenError(newData.error);
    }
  };

  useEffect(() => {
    const pusher = new Pusher(pusherKey, {
      cluster: "ap2",
      encrypted: true,
    });
    // console.log(pusher);
    const channel = pusher.subscribe(`ch-${props.id}`);
    getNotificationCount(channel);
    getNotificationList();
    let x = localStorage.getItem("nut_count");
    if (x) {
      setNotificationCount(x);
    } else {
      setNotificationCount(notificationListCount);
    }
  }, []);

  useEffect(() => {
    if (parseInt(notificationCount) == 0 && notificationList.length > 0) {
      setNotificationList([]);
    }
  }, [setNotificationCount, notificationCount]);

  const toggleButton = (isOpen) => {
    if (isOpen) {
      setNotificationCount(0);
      localStorage.setItem("nut_count", 0);
    }
    // else {
    //   setNotificationList([]);
    // }
  };

  const notificationDelete = async (id) => {
    document.getElementById("dropdown-notification").click();
    const data = {
      url: "notification_history_delete",
      body: {
        id: id,
      },
    };
    const newData = await postMethodNoLoad(data);
    // console.log(newData);
    if (newData.status === 1) {
      // update chat count
    }
  };

  const stringLimit = (string) => {
    let sub = "";
    if (stringLength >= string.length) {
      sub = string;
    } else {
      sub = string.substring(0, stringLength) + "...";
    }
    return `${sub}`;
  };

  return (
    <div className="">
      <Dropdown
        className="notificationHeader userNameBox d-inline-block mr-3"
        // onToggle={(isOpen) => toggleButton(isOpen)}
      >
        <Dropdown.Toggle
          variant="link"
          id="dropdown-notification"
          className="text-capitalize px-1 py-3"
        >
          <>
            <span
              className={notificationCount > 0 ? "nt-count" : "nt-count hide"}
            >
              {notificationCount}
            </span>
            <FontAwesomeIcon className="f-30" icon={faBell} />
          </>
        </Dropdown.Toggle>
        <Dropdown.Menu className="min-16">
          {notificationList.length == 0 ? (
            <ul className="customScrollBar">
              <li>
                <a className="py-3" href="javascript:void(0)">
                  No Notification
                </a>
              </li>
            </ul>
          ) : (
            <ul className="customScrollBar list">
              {notificationList.map((item, index) => (
                <li key={index}>
                  <Link
                    to={{
                      pathname:
                        item.target_screen == "Meal Tracker Comments"
                          ? `/meal-tracker`
                          : "/user-chat",
                      key: `header-${item.user_id}`,
                      state: {
                        user_id: item.user_id,
                        user_image_url: item.get_username.image_url,
                        user_name: item.get_username.first_name
                          ? item.get_username.first_name
                          : "User",
                      },
                    }}
                    className="dropdown-item btn btn-link"
                    key={index}
                    onClick={() => notificationDelete(item.id)}
                  >
                    {stringLimit(item.message)}
                  </Link>
                </li>
              ))}
            </ul>
          )}
        </Dropdown.Menu>
      </Dropdown>
    </div>
  );
}
