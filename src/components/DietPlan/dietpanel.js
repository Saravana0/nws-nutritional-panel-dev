import React from "react";
import PhotoImage from "../../assets/images/photo.png";
import { Button, Form, Col, Row, Tabs, Tab } from "react-bootstrap";
import "./dietpanelmenu.css";

function DietPanel() {
  return (
    <div>
      <div className="dietpanel_sub_box">
        <div className="d-flex dishes_boxes">
          <div className="d-flex flex-column">
            <Col className="dishes">
              <h5 className=" mb-3 pt-3">Dish Name</h5>
              <input type="text" id="dishName" placeholder="Oats" />
            </Col>
            <Col className="dishes">
              <h5 className="mb-3">Dish Type</h5>
              <input type="text" id="dishName" placeholder="Optional" />
            </Col>
          </div>
          <div className="gallery-img">
            <img src={PhotoImage} alt={"photos"} />
          </div>
        </div>

        <h5 className="ml-5 mb-3">Discription</h5>
        <textarea rows="4" cols="1" className="description-area ml-5 ">
          Lorem ipsum dolor sisec scing Aenean massa. Cum sociis natoque
          penatibus et magnis dis parturient montes, nascetur ridiculus mus.
          Donec quam felis, ultricies nec, pellentesque eu
        </textarea>
        <h5 className="ml-5 mb-3 mt-3">Replacement/others</h5>
        <textarea
          rows="2"
          cols="1"
          className="description-area ml-5 mb-3 "
        ></textarea>
      </div>
    </div>
  );
}

export default DietPanel;
