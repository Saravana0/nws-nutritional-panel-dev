import { React, useState } from "react";
import { Button, Form, Col, Row, Tabs, Tab } from "react-bootstrap";
import DietPanel from "./dietpanel";
import './dietpanelmenu.css';

function DietpanelMenu(props) {
  const [key, setKey] = useState("1");
  return (
    <div>
      <Row>
        <Col>
          <div className="sub-contant">
            <Tabs
              id="progress-tracker-tab"
              activeKey={key}
              onSelect={(k) => setKey(k)}
              className="dietpanel-menu"
            >
              <Tab eventKey="1" title="On Waking">
                <div className="dietpanel-box">
                  <DietPanel />
                </div>
              </Tab>
              <Tab eventKey="2" title="Pre- Breakfast">
                <div className="dietpanel-box">
                  <DietPanel />
                </div>
              </Tab>
              <Tab eventKey="3" title="Breakfast">
                <div className="dietpanel-box">
                  <DietPanel />
                </div>
              </Tab>
              <Tab eventKey="4" title="Morning Snack">
                <div className="dietpanel-box">
                  <DietPanel />
                </div>
              </Tab>
              <Tab eventKey="5" title="Lunch">
                <div className="dietpanel-box">
                  <DietPanel />
                </div>
              </Tab>
              <Tab eventKey="6" title="Evening Snack">
                <div className="dietpanel-box">
                  <DietPanel />
                </div>
              </Tab>
              <Tab eventKey="7" title="Dinner">
                <div className="dietpanel-box">
                  <DietPanel />
                </div>
              </Tab>
              <Tab eventKey="8" title="Post Dinner">
                <div className="dietpanel-box">
                  <DietPanel />
                </div>
              </Tab>
            </Tabs>
          </div>
        </Col>
      </Row>
    </div>
  );
}

export default DietpanelMenu;
